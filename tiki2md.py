#! /usr/bin/env python2.7

import re
import sys
import os

from os import listdir
from os.path import isfile, join


if len(sys.argv) > 1:
    sourcepath = sys.argv[1]
else:
    sourcepath = 'source/'

# take the restricted filename as parameter
# ToDo: move the filename to Config
if len(sys.argv) > 2:
    restricted_filename = sys.argv[2]
else:
    restricted_filename = "restrictedpages.txt"

# Read the restricted pagenames from file
restrictedpages=[]
if os.path.isfile(restricted_filename):
    with open(restricted_filename) as file:
        for line in file:
            restrictedpages.append(line.strip()) #preprocess line
exclude_dirs =set(['.git'])
for root, dirs, files in os.walk( sourcepath, topdown=True, onerror=None ):
    dirs[:] = [d for d in dirs if d not in exclude_dirs]
    print dirs

    for name in files:
        relative_filename = os.path.join(root, name)
        sourcefile = open( relative_filename , 'r+')
        content = sourcefile.read()

        searchreplaces = [
            [ r'^!!!!!!\s?'         , r'###### '] ,   # heading6
            [ r'^!!!!!\s?'          , r'##### '] ,    # heading5
            [ r'^!!!!\s?'           , r'#### '] ,     # heading4
            [ r'^!!!\s?'            , r'### '] ,      # heading3
            [ r'^!!\s?'             , r'## '] ,       # heading2
            [ r'^!\s?'              , r'# '] ,        # heading1

            [ r'{img src=(.*?)\s?}' , r'![\1](\1)'],  # image

            [ r'\*\*\s?'            , r'  * '] ,      # intended list
            [ r'\*\*\*\s?'          , r'    * '] ,    # intended intended list
            ## TODO list needs an leading blank line in markdown


            [ r'\[(.*?)\|(.*?)\]'   , r'[\2](\1)'] ,  # external links
            [ r'\(\((.*?)\|(.*?)\)\)'    , r'[\2](./\1)'] ,    # internal links with pipes
            [ r'\(\((.*?)\)\)'      , r'[[\1]]'] ,    # internal links
            [ r'\[\[p\:(.*?)]]'     , r'[\1](https://de.wikipedia.org/wiki/\1)' ] ,    # wikipedia links
            [ r'\[\[ped\:(.*?)]]'   , r'[\1](https://de.wikipedia.org/wiki/\1)' ] ,    # wikipedia links

            [ r'\{maketoc\}'        , r''] ,          # rm toc
        ]

        for searchreplace in searchreplaces:
            content = re.sub(searchreplace[0], searchreplace[1], content ,  flags=re.MULTILINE)

        # create the relative pagename, independent from the absolut path
        pagename = os.path.relpath(relative_filename, sourcepath)
        if pagename in restrictedpages:
            content = content + "\n#meta#\npublish: false"

        sourcefile.seek(0)
        sourcefile.write( content )
        sourcefile.truncate()
        sourcefile.close()
