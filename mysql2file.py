#!/usr/bin/env python
# -*- coding: utf-8 -*-


# to get html intead wiki text use: 
# python mysql2file.py cache
# works only for non protected pages


import MySQLdb
import re
import os
import codecs
import ConfigParser
import sys

### config
sourceroot      = 'source/'
databasename    = 'umija_umijaorg'
# https://dev.mysql.com/doc/refman/5.7/en/option-files.html
configfile      = '/home/umija/.my.cnf'
### /config

config = ConfigParser.ConfigParser()
config.read( configfile )

db = MySQLdb.connect(host=      "localhost",    # your host, usually localhost
                     user=      config.get('client', 'user'),         # your username
                     passwd=    config.get('client', 'password'),  # your password
                     db=        databasename, # name of the data base
                     charset=   'utf8'
                     )        


cur = db.cursor()

cur.execute("SELECT pageName,description,data FROM tiki_pages")


sourcefiles = []

for source in cur.fetchall():

    if source[2] == None :
	continue

    sourcepath = sourceroot + source[0].replace(":", "/" ).replace(" ", "_" ) 

    directory = os.path.dirname( sourcepath )

    if not os.path.exists(directory):
        os.makedirs(directory)

    sourcefile = codecs.open( sourcepath + '.md' , 'w+', "utf-8")

    ## description as heading1
    sourcefile.write( '# ' + source[1] )
    sourcefile.write(  '\n\n' )
    ## content
    sourcefile.write( source[2] )
    sourcefile.close()

db.close()


